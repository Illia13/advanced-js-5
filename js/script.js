class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
    this.element = this.createCard();
  }

  createCard() {
    const card = document.createElement("div");
    card.classList.add("card");

    const title = document.createElement("h3");
    title.textContent = this.post.title;

    const body = document.createElement("p");
    body.textContent = this.post.body;

    const author = document.createElement("div");
    author.classList.add("author");
    author.textContent = `${this.user.name} (${this.user.email})`;

    const deleteBtn = document.createElement("button");
    deleteBtn.classList.add("delete-btn");
    deleteBtn.textContent = "Delete";
    deleteBtn.addEventListener("click", () => this.deleteCard());

    card.appendChild(title);
    card.appendChild(body);
    card.appendChild(author);
    card.appendChild(deleteBtn);

    return card;
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: "DELETE",
    }).then((response) => {
      if (response.ok) {
        this.element.remove();
      } else {
        alert("Failed to delete the post");
      }
    });
  }

  render(parent) {
    parent.appendChild(this.element);
  }
}

document.addEventListener("DOMContentLoaded", () => {
  const feed = document.getElementById("feed");

  Promise.all([
    fetch("https://ajax.test-danit.com/api/json/users").then((res) =>
      res.json()
    ),
    fetch("https://ajax.test-danit.com/api/json/posts").then((res) =>
      res.json()
    ),
  ])
    .then(([users, posts]) => {
      posts.forEach((post) => {
        const user = users.find((user) => user.id === post.userId);
        const card = new Card(post, user);
        card.render(feed);
      });
    })
    .catch((error) => console.error("Error fetching data:", error));
});
